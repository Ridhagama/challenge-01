4
function isValidPassword(givenPassword) {
  const regex = /(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z])/;
  if (typeof givenPassword === 'string') {
    if (regex.test(givenPassword) && givenPassword.length >=8){ 
      return true;
    } 
    else { 
      return false; 
    }
  } 
  else if (typeof givenPassword === 'undefined') {
    return 'ERROR : Tolong Masukan Password';
  } 
  else {
    return 'ERROR : invalid data type';
  }
}

console.log(isValidPassword('Meong2021'))
console.log(isValidPassword('meong2021'))
console.log(isValidPassword('@eong'))
console.log(isValidPassword('Meong2'))
console.log(isValidPassword(0)) // tidak sesuai dengan tipe data yaitu 'string'.
console.log(isValidPassword()) // tidak memenuhi parameter (givenPassword).
