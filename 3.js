function checkEmail(email=-1){
    let at = /[@]/g
    let domain = /.com|.co.id/g
    if(email==-1 ) return "ERROR";
    if(email.search(at)!=-1){
        if(email.search(domain)!=-1){
            return "VALID";
        }else{
            return "INVALID";
        }
    }
    return "ERROR";
}

console.log(checkEmail('apranata@binar.co.id'))
console.log(checkEmail('apranata@binar.com'))
console.log(checkEmail('apranata@binar'))
console.log(checkEmail('apranata')) // tidak lengkap
console.log(checkEmail(3322)) // tidak memenuhi tipe string
console.log(checkEmail()) // tidak memenuhi parameter
