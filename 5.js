function getSplitName(personName) {
    if (typeof personName === 'string') {
    
      const name = personName.split(' ');
        if (name.length <= 3) {
        if (name.length <= 2) {
              return {
                firstName : name[0],
                middleName : null,
                lastName : typeof name [1] === 'undefined' ? null : name[1],
          };
        }	else 
              return {
                 firstName : name[0],
                 middleName : name[1],
                 lastName : name[2],
         };
      } else {
        return 'Error : This Function is only for 3 characther name' 
      }
      
    } else {
    return 'Error : invalid data type';
    }
  }  
      
  console.log(getSplitName("Aldi Daniella Pranata"))
  console.log(getSplitName("Dwi Kuncoro"))
  console.log(getSplitName("Aurora"))
  console.log(getSplitName("Aurora Aureliya Sukma Darma"))
  console.log(getSplitName(0)) // tidak memenuhi tipe data